variable "cobbler_user" {}
variable "cobbler_pass" {}
variable "cobbler_url" {}

provider "cobbler" {
  username = "${var.cobbler_user}"
  password = "${var.cobbler_pass}"
  url      = "${var.cobbler_url}"
}

resource "cobbler_distro" "centos-7-x86_64" {
  name       = "centos-7-x86_64"
  breed      = "redhat"
  os_version = "rhel7"
  arch       = "x86_64"
  kernel     = "/var/lib/cobbler/kernels/centos/vmlinuz"
  initrd     = "/var/lib/cobbler/kernels/centos/initrd.img"
}

resource "cobbler_profile" "centos-7-ws" {
  name      = "centos-7-ws"
  distro    = "${cobbler_distro.centos-7-x86_64.name}"
  kickstart = "${cobbler_kickstart_file.centos-7-ws-ks.name}"
  ks_meta   = "url=http://mirrors.mit.edu/centos/7/os/x86_64"
}

resource "cobbler_system" "test" {
  name      = "test"
  profile   = "${cobbler_profile.centos-7-ws.name}"
  comment   = "This is a laptop"
  kickstart = "<<inherit>>"
  ks_meta   = "<<inherit>>"

  interface {
    name        = "eth1"
    mac_address = "52:54:00:34:e9:82"
    static      = true
    ip_address  = "192.168.125.3"
    netmask     = "255.255.255.0"
  }
}

resource "cobbler_kickstart_file" "centos-7-ws-ks" {
  name = "/var/lib/cobbler/kickstarts/centos-7-ws.ks"
  body = "${file("centos-7-ws.ks")}"
}
