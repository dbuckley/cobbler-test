#version=DEVEL

%pre
#----- Setup disk partitioning -----
if [ -d /sys/block/nvme0 ]; then
    BOOTDRIVE="nvme0"
elif [ -d /sys/block/vda ]; then
    BOOTDRIVE="/dev/vda"
else
    BOOTDRIVE="/dev/sda"
fi

cat << EOF > /tmp/dynamic-include
ignoredisk --only-use="\$BOOTDRIVE"
zerombr

clearpart --all --drives="\$BOOTDRIVE" --initlabel
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive="\$BOOTDRIVE"
part /boot/efi --fstype="efi" --ondisk="\$BOOTDRIVE" --size=200 --fsoptions="umask=0077,shortname=winnt"
part /boot --fstype="xfs" --ondisk="\$BOOTDRIVE" --size=1024
part pv.311 --fstype="lvmpv" --ondisk="\$BOOTDRIVE" --size=20500 --grow
volgroup centos --pesize=4096 pv.311
logvol /  --fstype="xfs" --size=16384 --grow --name=root --vgname=centos
logvol swap  --fstype="swap" --size=4096 --name=swap --vgname=centos

EOF

echo "\$(ip l | grep -o 'e[a-z0-9]\{3,7\}' | grep '[0-9]')" > /tmp/net-list
while read net; do
    echo "network --bootproto=dhcp --device=\$net --onboot=on --ipv6=auto" >> /tmp/dynamic-include
done < /tmp/net-list

%end

# Set sha512 for passwords
auth --enableshadow --passalgo=sha512

# Run text based install and use repo for latest packages
text
cdrom

keyboard --vckeymap=us --xlayouts='us'
lang en_US.UTF-8
timezone America/New_York --isUtc

# Halt system without asking after install
poweroff

# Import dynamically created ks portion
%include /tmp/dynamic-include

# Network hostname
network  --hostname=changeme

# Set root and hpcadmin
rootpw --iscrypted \$6\$7IXNwdrEXMijuH5s\$T/.NtF99GAUcosiTYW275MT//KChVyQkw5yPzP.NBN5Bhl3iROwMcGb8zGBLkYvt3c1A3.0dC7MU3VGEmQtDn/
user --groups=wheel --name=hpcadmin --password=\$6\$Rm8VSPcOIJNxiygo\$ffXWAJqOkUj.7IjDSwlyqBd/xEpMJTeVBirZ/ikth49ztMjTbj4n8XrvdXL3/4bgg18upkVPnAlVRzGBm0jVA0 --iscrypted --gecos="hpcadmin"

# System services
services --enabled="chronyd"


%packages
@base
@compat-libraries
@core
@python
chrony
kexec-tools

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

%anaconda
pwpolicy root --minlen=6 --minquality=1 --notstrict --nochanges --notempty
pwpolicy user --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy luks --minlen=6 --minquality=1 --notstrict --nochanges --notempty
%end
